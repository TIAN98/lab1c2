/*
 * File:   main.c
 * Author: Andres jacob
 *
 * Created on 21 de octubre de 2021, 02:34 PM
 */


//#include "xc.h"
#include <stdio.h>
#include <stdlib.h>
#define FCY 5000000
#include <libpic30.h>
#include "config.h"



int cont=0;
int band=0;

void main () {
    
    // CONFIGURACION PUERTOS
    TRISBbits.TRISB7=1; //entradas

    //salidas
    TRISBbits.TRISB14=0; //G
    TRISBbits.TRISB15=0; //F
    TRISBbits.TRISB11=0; //E
    TRISBbits.TRISB10=0; //D
    TRISBbits.TRISB13=0; //C
    TRISBbits.TRISB2=0;  //B
    TRISBbits.TRISB3=0;  //A    
    
    PORTB=0X000;
    
    while(1){
        boton1();
        direccion();
        imprimir();
    }    
}

void boton1(){
    if(PORTBbits.RB7==0){
        while(PORTBbits.RB7==0){}
        band=!band;   
    }
}

void direccion(){
    if (band ==1){
        sumar();
    }
    if(band==0){
        restar();
    }
}

void sumar(){
    cont=cont+1;
    boton1();
    if (cont==10){
        cont=0;
    }
}

void restar (){
    cont=cont-1;
    boton1();
    if (cont==-1){
        cont=9;
    }
}

void imprimir (){
    if (cont==0){
    cero();
    boton1();
    }
    if (cont==1){
    uno();
    boton1();
    }
    if (cont==2){
    dos();
    boton1();
    }
    if (cont==3){
    tres();
    boton1();
    }
    if (cont==4){
    cuatro();
    boton1();
    }
    if (cont==5){
    cinco();
    boton1();
    }
    if (cont==6){
    seis();
    boton1();
    }
    if (cont==7){
    siete();
    boton1();
    }
    if (cont==8){
    ocho();
    boton1();
    }
    if (cont==9){
    nueve();
    boton1();
    }
}

void uno () {
    LATBbits.LATB14=0; //G
    LATBbits.LATB15=0; //F
    LATBbits.LATB11=0; //E
    LATBbits.LATB10=0; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=0;  //A
    __delay_ms(100);
}

void dos () {
    LATBbits.LATB14=1; //G
    LATBbits.LATB15=0; //F
    LATBbits.LATB11=1; //E
    LATBbits.LATB10=1; //D
    LATBbits.LATB13=0; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=1;  //A
    __delay_ms(100);
}

void tres () {
    LATBbits.LATB14=1; //G
    LATBbits.LATB15=0; //F
    LATBbits.LATB11=0; //E
    LATBbits.LATB10=1; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=1;  //A 
    __delay_ms(100);
}

void cuatro () {
    LATBbits.LATB14=1; //G
    LATBbits.LATB15=1; //F
    LATBbits.LATB11=0; //E
    LATBbits.LATB10=0; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=0;  //A 
    __delay_ms(100);   
}

void cinco () {
    LATBbits.LATB14=1; //G
    LATBbits.LATB15=1; //F
    LATBbits.LATB11=0; //E
    LATBbits.LATB10=1; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=0;  //B
    LATBbits.LATB3=1;  //A 
    __delay_ms(100);    
}

void seis () {
    LATBbits.LATB14=1; //G
    LATBbits.LATB15=1; //F
    LATBbits.LATB11=1; //E
    LATBbits.LATB10=1; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=0;  //B
    LATBbits.LATB3=1;  //A 
    __delay_ms(100);
}

void siete () {
    LATBbits.LATB14=0; //G
    LATBbits.LATB15=0; //F
    LATBbits.LATB11=0; //E
    LATBbits.LATB10=0; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=1;  //A 
    __delay_ms(100);
}

void ocho () {
    LATBbits.LATB14=1; //G
    LATBbits.LATB15=1; //F
    LATBbits.LATB11=1; //E
    LATBbits.LATB10=1; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=1;  //A 
    __delay_ms(100);    
}

void nueve () {
    LATBbits.LATB14=1; //G
    LATBbits.LATB15=1; //F
    LATBbits.LATB11=0; //E
    LATBbits.LATB10=0; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=1;  //A 
    __delay_ms(100);    
}

void cero () {
    LATBbits.LATB14=0; //G
    LATBbits.LATB15=1; //F
    LATBbits.LATB11=1; //E
    LATBbits.LATB10=1; //D
    LATBbits.LATB13=1; //C
    LATBbits.LATB2=1;  //B
    LATBbits.LATB3=1;  //A 
    __delay_ms(100);    
}