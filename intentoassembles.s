;INCLUIR UN ARCHIVO DE CABECERA

	    
.global _main
;dentro del main establecer la configuracion
	    
_main:	    
	    
    MOV #0XFFFF,W0
    MOV W0, AD1PCFG ; DESACTIVA LOS CONVERSORES ANALOGOS
    MOV #0X0000, W2
    MOV W2, ODCA
    MOV #0X0000, W2
    MOV W2, ODCB
    MOV #0X0000, W1
    MOV W1, TRISB
    MOV #0X000, W0
    MOV W0, PORTB

    ;Definir Variables
    BSET TRISB, #0X5 ;defino el pin del registro RB5 como entrada
    BCLR TRISB, #0x6 ;defino el pin de rstro RB6 como salida 
    MOV #0X0, W9; global direccion
    MOV #0X0, W10; global numero
	    
_bucle:    
    CALL _direc
   
    GOTO _bucle
	
_arriba:
    CALL _uno
    CALL _negador
    GOTO _bucle

    
_abajo:
    CALL _dos
    CALL _negador
    GOTO _bucle


_direc:
    
    MOV #0X01,W3
    SUB W3,W9,W4 ;si da 0 va arriba si da otro resultado va a abajo
    BRA Z,_arriba
    GOTO _abajo
    
    
 _negador:
    
    NEG W9,W11    ;niego W9 y se guarda en W6
    MOV #0X01,W3
    SUB W11,W3,W9 ;Actualizo la variable negada
    RETURN
    
	    
_uno:
    ;MOSTRAR 1 POR 1 SEG
	    BCLR    LATB, #0xE ;G
	    BCLR    LATB, #0XF ;F
	    BCLR    LATB, #0XB ;E
	    BCLR    LATB, #0XA ;D
	    BSET    LATB, #0XD ;C
	    BSET    LATB, #0X2 ;B
	    BCLR    LATB, #0X3 ;A
	    CALL    DELAY1S
	    
	    ;MOV #0X00, W4
	    RETURN 
_dos:
    ;MOSTRAR 2 POR 2 SEG    
	    BSET    LATB, #0xE ;G
	    BCLR    LATB, #0XF ;F
	    BSET    LATB, #0XB ;E
	    BSET    LATB, #0XA ;D
	    BCLR    LATB, #0XD ;C
	    BSET    LATB, #0X2 ;B
	    BSET    LATB, #0X3 ;A
	    CALL    DELAY1S
	    
	    ;MOV #0X02, W4
	    RETURN
_tres:
    ;MOSTRAR 3 POR 3 SEG	    
	    BSET    LATB, #0xE ;G
	    BCLR    LATB, #0XF ;F
	    BCLR    LATB, #0XB ;E
	    BSET    LATB, #0XA ;D
	    BSET    LATB, #0XD ;C
	    BSET    LATB, #0X2 ;B
	    BSET    LATB, #0X3 ;A
	    CALL    DELAY1S
	    
	    ;MOV #0X03, W4
	    RETURN  
	   
_cuatro:
    ;MOSTRAR 4 POR 4 SEG	    
	    BSET    LATB, #0xE ;G
	    BSET    LATB, #0XF ;F
	    BCLR    LATB, #0XB ;E
	    BCLR    LATB, #0XA ;D
	    BSET    LATB, #0XD ;C
	    BSET    LATB, #0X2 ;B
	    BCLR    LATB, #0X3 ;A
	    CALL    DELAY1S
	    
	    ;MOV #0X04, W4
	    RETURN  

	    
_cinco:
    ;MOSTRAR 5 POR 5 SEG	    
	    BSET    LATB, #0xD ;G
	    BSET    LATB, #0XF ;F
	    BCLR    LATB, #0XB ;E
	    BSET    LATB, #0XA ;D
	    BSET    LATB, #0XD ;C
	    BCLR    LATB, #0X2 ;B
	    BSET    LATB, #0X3 ;A	    
	    CALL    DELAY1S
	    
	    ;MOV #0X05, W4
	    RETURN  
	   

_seis:
    ;MOSTRAR 6 POR 6 SEG	    
	    BSET    LATB, #0xD ;G
	    BSET    LATB, #0XF ;F
	    BSET    LATB, #0XB ;E
	    BSET    LATB, #0XA ;D
	    BSET    LATB, #0XD ;C
	    BCLR    LATB, #0X2 ;B
	    BSET    LATB, #0X3 ;A
	    CALL    DELAY1S
	    
	    ;MOV #0X06, W4
	    RETURN
_siete:
    ;MOSTRAR 7 POR 7 SEG	    
	    BCLR    LATB, #0xE ;G
	    BCLR    LATB, #0XF ;F
	    BCLR    LATB, #0XB ;E
	    BCLR    LATB, #0XA ;D
	    BSET    LATB, #0XD ;C
	    BSET    LATB, #0X2 ;B
	    BSET    LATB, #0X3 ;A
	    CALL    DELAY1S
	    ;MOV #0X07, W4
	    RETURN
_ocho:
    ;MOSTRAR 8 POR 8 SEG	    
	    BSET    LATB, #0xE ;G
	    BSET    LATB, #0XF ;F
	    BSET    LATB, #0XB ;E
	    BSET    LATB, #0XA ;D
	    BSET    LATB, #0XD ;C
	    BSET    LATB, #0X2 ;B
	    BSET    LATB, #0X3 ;A
	    CALL    DELAY1S
	    ;MOV #0X08, W4
	    RETURN
_nueve:	    
;MOSTRAR 9 POR 10 SEG	    
	    BSET    LATB, #0xE ;G
	    BSET    LATB, #0XF ;F
	    BCLR    LATB, #0XB ;E
	    BCLR    LATB, #0XA ;D
	    BSET    LATB, #0XD ;C
	    BSET    LATB, #0X2 ;B
	    BSET    LATB, #0X3 ;A
	    CALL    DELAY1S
	    ;MOV #0X09, W4
	    RETURN
_cero:    
	;MOSTRAR 0 POR 1 SEG	    
	    BCLR    LATB, #0xE ;G
	    BSET    LATB, #0XF ;F
	    BSET    LATB, #0XB ;E
	    BSET    LATB, #0XA ;D
	    BSET    LATB, #0XD ;C
	    BSET    LATB, #0X2 ;B
	    BSET    LATB, #0X3 ;A
	    CALL    DELAY1S
	    ;MOV #0X00, W4
	    RETURN
DELAY1S:
	    MOV	    #300,W0
REPETIR:
	    DEC	    W0, W1
	    MOV	    W1, W0
	    BRA	    Z, RETORNAR
	    REPEAT  #5000
	    NOP
	    GOTO    REPETIR
RETORNAR:
	    RETURN 
	    

	    
.END



